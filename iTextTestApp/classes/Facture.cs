﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iTextTestApp.classes
{
    public class Facture
    {
        public int Id { get; set; }
        public DateTime DateFacturation { get; set; }
        public int NumeroOrdre { get; set; }
        public int NumeroActivité { get; set; }
        public string Accueil { get; set; }
        public DateTime DatePaiement { get; set; }
        public int IdVehicule { get; set; }
        public Vehicule Vehicule { get; set; }
        public Dictionary<Element, ElementFacture> Elements { get; set; }
        private decimal? _totalHT;
        private decimal? _totalTVA;
        public decimal TotalHT
        {
            get
            {
                if (_totalHT == null)
                {
                    _totalHT = 0;
                    foreach(KeyValuePair<TVA, decimal> pair in MontantsHT)
                    {
                        _totalHT += pair.Value;
                    }
                }
                return (decimal)_totalHT;
            }
        }
        public decimal TotalTVA
        {
            get
            {
                if (_totalTVA == null)
                {
                    _totalTVA = 0;
                    foreach (KeyValuePair<TVA, decimal> pair in MontantsHT)
                    {
                        _totalTVA += pair.Value * pair.Key.Taux / 100;
                    }
                }
                return (decimal)_totalTVA;
            }
        }
        public decimal TotalTTC { get => TotalHT + TotalTVA; }
        private Dictionary<TVA, decimal> _montantsHT;
        public Dictionary<TVA, decimal> MontantsHT
        {
            get 
            {
                if (_montantsHT == null)
                {
                    _montantsHT = new Dictionary<TVA, decimal>();
                    foreach(KeyValuePair<Element, ElementFacture> pair in Elements)
                    {
                        Element element = pair.Key;
                        ElementFacture elementFacture = pair.Value;
                        TVA tva = element.TVA;
                        if (! _montantsHT.ContainsKey(tva))
                        {
                            _montantsHT.Add(tva, 0);
                        }
                        _montantsHT[tva] += elementFacture.MontantHT;
                    }
                }
                return _montantsHT;
            }
        }

        public decimal TotalPieces
        { 
            get 
            {
                decimal total = 0;
                foreach(KeyValuePair<Element, ElementFacture> pair in Elements)
                {
                    Element element = pair.Key;
                    ElementFacture elementFacture = pair.Value;
                    if (element.Type == TypeElement.Piece)
                    {
                        total += elementFacture.MontantHT;
                    }
                }
                total = Math.Round(total, 2);
                return total;
            }
        }
        public decimal TotalMO
        {
            get
            {
                decimal total = 0;
                foreach (KeyValuePair<Element, ElementFacture> pair in Elements)
                {
                    Element element = pair.Key;
                    ElementFacture elementFacture = pair.Value;
                    if (element.Type == TypeElement.MO)
                    {
                        total += elementFacture.MontantHT;
                    }
                }
                total = Math.Round(total, 2);
                return total;
            }
        }
        public decimal TotalForfait
        {
            get
            {
                decimal total = 0;
                foreach (KeyValuePair<Element, ElementFacture> pair in Elements)
                {
                    Element element = pair.Key;
                    ElementFacture elementFacture = pair.Value;
                    if (element.Type == TypeElement.Forfait)
                    {
                        total += elementFacture.MontantHT;
                    }
                }
                total = Math.Round(total, 2);
                return total;
            }
        }
    }
}
