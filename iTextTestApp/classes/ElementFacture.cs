﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iTextTestApp.classes
{
    public class ElementFacture
    {
        public int IdFacture { get; set; }
        public int IdElement { get; set; }
        public decimal Quantité { get; set; }
        public decimal PrixHTFacture { get; set; }
        public decimal Remise { get; set; }
        public decimal PrixNetHT 
        { 
            get 
            {
                return PrixHTFacture * (100 - Remise) / 100;
            } 
        }
        public decimal MontantHT
        {
            get
            {
                return PrixNetHT * Quantité;
            }
        }
    }
}
