﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iTextTestApp.classes
{
    public class Vehicule
    {
        public DateTime DateOR { get; set; }
        public int Kms { get; set; }
        public string Tvv { get; set; }
        public string Gamme { get; set; }
        public string Modele { get; set; }
        public string Immatriculation { get; set; }
        public string NumSérieFabrication { get; set; }
        public DateTime Date1ereMEC { get; set; }
        public DateTime DateLivr { get; set; }
        public Client Client { get; set; }
    }
}
